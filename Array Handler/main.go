package main

import (
	"fmt"
)

type ArrayProcessor interface {
	ProcessArray(arr []int) ([]int, int)
}

type SumAndZeroProcessor struct{}

func (sp *SumAndZeroProcessor) ProcessArray(arr []int) ([]int, int) {
	sum := 0
	newArr := make([]int, 0)

	for _, num := range arr {
		if num != 0 {
			newArr = append(newArr, num)
		}
		sum += num
	}

	return newArr, sum
}

type MultAndNegativeProcessor struct{}

func (mp *MultAndNegativeProcessor) ProcessArray(arr []int) ([]int, int) {
	product := 1
	newArr := make([]int, 0)

	for _, num := range arr {
		if num >= 0 {
			newArr = append(newArr, num)
		}
		product *= num
	}

	return newArr, product
}

func ProcessArray(arr []int, processor ArrayProcessor) ([]int, int) {
	return processor.ProcessArray(arr)
}

func main() {
	arr := []int{1, 2, 0, 3, -4, 5, -6, 8, 10, -11}

	sumAndZeroProcessor := &SumAndZeroProcessor{}
	newArr1, sum := ProcessArray(arr, sumAndZeroProcessor)
	fmt.Println("Sum and zero processor:\n")
	fmt.Println("New array:", newArr1)
	fmt.Println("Sum:", sum)

	fmt.Println("-----------------------")

	multAndNegativeProcessor := &MultAndNegativeProcessor{}
	newArr2, product := ProcessArray(arr, multAndNegativeProcessor)
	fmt.Println("Mult and negative processor:\n")
	fmt.Println("New array:", newArr2)
	fmt.Println("Product of elements:", product)
}
