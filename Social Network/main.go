package main

import (
	"fmt"
	"time"
)

type User struct {
	ID       int
	Username string
	Email    string
}

type Post struct {
	ID        int
	UserID    int
	Content   string
	Timestamp time.Time
}

type UserRepository interface {
	Save(user *User)
	GetByID(userId int) User
	Delete(userId int)
}

type PostRepository interface {
	Save(post *Post)
	GetByID(postID int) Post
	Delete(postID int)
}

//Тут должны быть реализации методов репозиториев

type UserService struct {
	userRepository UserRepository
}

func NewUserService(userRepository UserRepository) *UserService {
	return &UserService{
		userRepository: userRepository,
	}
}

func (s *UserService) CreateUser(username, email string) {
	user := &User{
		Username: username,
		Email:    email,
	}

	s.userRepository.Save(user)
}

func (s *UserService) GetUserByID(userID int) User {
	return s.userRepository.GetByID(userID)
}

func (s *UserService) Delete(userId int) {
	s.userRepository.Delete(userId)
}

type PostService struct {
	postRepository PostRepository
}

func NewPostService(postRepository PostRepository) *PostService {
	return &PostService{
		postRepository: postRepository,
	}
}

func (s *PostService) CreatePost(userID int, content string) {
	post := &Post{
		UserID:    userID,
		Content:   content,
		Timestamp: time.Now(),
	}

	s.postRepository.Save(post)
}

func (s *PostService) GetPostsByUserID(userId int) Post {
	return s.postRepository.GetByID(userId)
}

func (s *PostService) Delete(postID int) {
	s.postRepository.Delete(postID)
}

type UserController struct {
	userService *UserService
}

func NewUserController(userService *UserService) *UserController {
	return &UserController{
		userService: userService,
	}
}

func (c *UserController) CreateUser(username, email string) {
	c.userService.CreateUser(username, email)

	fmt.Println("Пользователь успешно создан")
}

func (c *UserController) GetUserByID(userId int) User {
	user := c.userService.GetUserByID(userId)

	fmt.Println("Пользователь успешно получен")
	return user
}

func (c *UserController) Delete(userId int) {
	c.userService.Delete(userId)

	fmt.Println("Пользователь успешно удален")
}

type PostController struct {
	postService *PostService
}

func NewPostController(postService *PostService) *PostController {
	return &PostController{
		postService: postService,
	}
}

func (c *PostController) CreatePost(postId int, content string) {
	c.postService.CreatePost(postId, content)

	fmt.Println("Пост успешно создан")
}

func (c *PostController) GetPostsByUserID(postId int) Post {
	post := c.postService.GetPostsByUserID(postId)

	fmt.Println("Пост успешно получен")

	return post
}

func (c *PostController) Delete(postId int) {
	c.postService.Delete(postId)

	fmt.Println("Пост успешно удален")
}

func main() {
	userService := NewUserService(nil)

	userController := NewUserController(userService)

	userController.CreateUser("Lex", "lex@mail.ru")

	user := userService.GetUserByID(1)

	fmt.Println("Полученный пользователь:", user)

	postService := NewPostService(nil)

	postController := NewPostController(postService)

	postController.CreatePost(1, "Lex Post")

	post := postService.GetPostsByUserID(1)

	fmt.Println("Полученный пост пользователя:", post)
}
